package com.bbva.hackaton.service.impl;

import com.bbva.hackaton.model.Cliente;
import com.bbva.hackaton.repository.ClienteRepository;
import com.bbva.hackaton.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public Cliente registrarCliente(Cliente cliente) {
        return clienteRepository.insert(cliente);
    }

    public Cliente obtenerClientePorCodCentral(String codCentral) {
        List<Cliente> clientes = clienteRepository.findAll();
        Optional<Cliente> c = clientes.stream()
                .filter(cliente -> codCentral.equals(cliente.getCodCentral()))
                .findFirst();
        return c.isPresent() ? c.get() : null;
    }

    @Override
    public Cliente obtenerClientePorNumDoc(String tipoDoc, String numDoc) {
        List<Cliente> clientes = clienteRepository.findAll();
        Optional<Cliente> c = clientes.stream()
                .filter(p -> p.getCodCentral() != null && numDoc.equals(p.getNumDoc()) && tipoDoc.equals(p.getTipoDoc()))
                .findFirst();
        return c.isPresent() ? c.get() : null;
    }

    @Override
    public boolean delete(Cliente cliente) {
        try {
            clienteRepository.delete(cliente);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }

    @Override
    public Cliente actualizarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }
}
