package com.bbva.hackaton.service;

import com.bbva.hackaton.model.Cliente;

public interface ClienteService {

    //POST /clientes
    Cliente registrarCliente(Cliente cliente);

    //GET /clientes/{codCentral}
    Cliente obtenerClientePorCodCentral(String codCentral);

    //GET /clientes?tipoDoc=XXX&numDoc=XXX
    Cliente obtenerClientePorNumDoc(String tipoDoc, String numDoc);

    //DELETE /clientes/{codCentral}
    boolean delete(Cliente cliente);

    //PATCH /clientes/{codCentral} @RequestBody
    Cliente actualizarCliente(Cliente cliente);
}
