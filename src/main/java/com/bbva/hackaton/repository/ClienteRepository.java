package com.bbva.hackaton.repository;

import com.bbva.hackaton.model.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClienteRepository extends MongoRepository<Cliente, String> {

}