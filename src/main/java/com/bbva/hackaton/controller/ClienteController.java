package com.bbva.hackaton.controller;

import com.bbva.hackaton.model.Cliente;
import com.bbva.hackaton.model.CelularCliente;
import com.bbva.hackaton.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente postCliente(@RequestBody Cliente cliente) {
        return clienteService.registrarCliente(cliente);
    }

    @GetMapping("/{codCentral}")
    public Cliente getClientePorId(@PathVariable String codCentral){
        return clienteService.obtenerClientePorCodCentral(codCentral);
    }

    @GetMapping
    public ResponseEntity<Cliente> getClientePorDocumento(@RequestParam(value = "tipoDoc", required = false) String tipoDoc,
                                                          @RequestParam(value = "numDoc", required = false) String numDoc){
        Cliente c = clienteService.obtenerClientePorNumDoc(tipoDoc, numDoc);
        if (c != null) {
            return ResponseEntity.ok(c);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{codCentral}")
    public boolean deleteCliente(@PathVariable String codCentral){
        Cliente c = clienteService.obtenerClientePorCodCentral(codCentral);
        if (c != null) {
            return clienteService.delete(c);
        }
        return false;
    }

    @PatchMapping("/{codCentral}")
    public void updateTelefono(@PathVariable String codCentral, @RequestBody CelularCliente celular){
        Cliente c = clienteService.obtenerClientePorCodCentral(codCentral);
        c.setCelular(celular.getCelularNuevo());
        clienteService.actualizarCliente(c);
    }
}
